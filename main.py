from datetime import datetime
import logging
import sys
from discord import Bot
import urllib.request
import os

logging_format = '%(asctime)s.%(msecs)03d :: %(levelname)s :: %(message)s'
logging.basicConfig(level=logging.WARNING, format=logging_format)
LOGGER = logging.getLogger('ai')
LOGGER.setLevel(logging.DEBUG)

bot = Bot()


@bot.event
async def on_ready():
    await bot.change_presence(status='omnomnom eating those GIFs')

    files_downloaded = 0

    for channel in bot.guilds[0].channels:
        if channel.name != 'gif-hole':
            continue

        LOGGER.info('Beginning to read channel gif-hole')
        reading = True
        before = datetime.now()
        while reading:
            reading = False
            LOGGER.info(f'reading messages before {before}')
            async for message in channel.history(before=before):
                # continue reading if there was at least one message to read
                reading = True
                before = message.created_at

                for embed in message.embeds:
                    if embed.type == 'gifv':
                        target_name = 'data/' + embed.video.url.split('/')[-1]
                        if not os.path.isfile(target_name):
                            LOGGER.info(f'attempting to download {embed.video.url}...')
                            try:
                                urllib.request.urlretrieve(embed.video.url, target_name)
                            except Exception:
                                LOGGER.error(f'download of {embed.video.url} failed')
                                continue
                            else:
                                LOGGER.info('done')

                            files_downloaded += 1

    LOGGER.info(f'downloaded {files_downloaded} files')
    await bot.close()


bot.run(sys.argv[1])
